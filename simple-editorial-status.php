<?php
/**
 * Plugin Name: Simple Editorial Status
 * Plugin URI: https://www.studiosilfverqvist.se
 * Description: Adds a simple editorial status selector.
 * Version: 1.0
 * Author: Viktor Silfverhjelm Hultqvist
 * Author URI: https://www.studiosilfverqvist.se
 * License: GPL2
 */


/**
 * Register meta boxes.
 */
function ses_register_meta_boxes() {
	global $screens;

    add_meta_box( 
    	'ses-1', __( 'Redaktionell status', 'wp-ses' ), 
    	'ses_display_callback', 
    	get_post_types(),
    	'side' );
}
add_action( 'add_meta_boxes', 'ses_register_meta_boxes' );

/**
 * Meta box display callback.
 *
 * @param WP_Post $post Current post object.
 */
function ses_display_callback( $post ) { 

	$status = esc_attr( get_post_meta( get_the_ID(), 'ses_status', true ) );
	$comment = esc_attr( get_post_meta( get_the_ID(), 'ses_comment', true ) );


	?>
	<div class="hcf_box">
	    <style scoped>
	       
	    </style>
	    <p class="meta-options ses_field">
	        <label for="ses_status">Status</label>
	        <select id="ses_status" type="select" name="ses_status" style="width:100%">
	        	<option value="unfinished" <?php selected( $status, 'unfinished', true ); ?>><?php _e('Ej påbörjad', 'wp-ses'); ?></option>
	        	<option value="needsmore" <?php selected( $status, 'needsmore', true ); ?>><?php _e('Behöver mer', 'wp-ses'); ?></option>
	        	<option value="complete"<?php selected( $status, 'complete', true ); ?> ><?php _e('Färdig', 'wp-ses'); ?></option>

	        </select>
	    </p>
	    <p class="meta-options ses_comment">
	        <label for="ses_comment">Kommentar</label>
	        <input id="ses_comment" type="text" name="ses_comment" style="width:100%" value="<?php echo $comment; ?>">
	    </p>
	</div>
<?php }

function ses_save_meta_box( $post_id ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    if ( $parent_id = wp_is_post_revision( $post_id ) ) {
        $post_id = $parent_id;
    }
    $fields = [
        'ses_status',
        'ses_comment'
    ];
    foreach ( $fields as $field ) {
        if ( array_key_exists( $field, $_POST ) ) {
            update_post_meta( $post_id, $field, sanitize_text_field( $_POST[$field] ) );
        }
     }
}
add_action( 'save_post', 'ses_save_meta_box' );


function ses_meta_column ( $columns ) {
   	$columns['ses_meta'] = __( 'Redaktionell status', 'wp-ses' );
    return $columns;
}
add_filter( 'manage_pages_columns',  'ses_meta_column' );

add_action('init', 'ses_loop_add_columns', 20);
function ses_loop_add_columns() {
	$pt = get_post_types();
	foreach( $pt as $ptype) {
		$filter = 'manage_edit-'.$ptype.'_columns';
		add_filter( $filter, 'ses_meta_column');
		
	}

}

add_action('init', 'ses_loop_populate_columns', 1);
function ses_loop_populate_columns() {
	$pt = get_post_types();
	foreach( $pt as $ptype) {
		$action = 'manage_'.$ptype.'_posts_custom_column';
		$filter = 'manage_edit-'.$ptype.'_sortable_columns';
		add_action ( $action, 'ses_populate_columns', 1, 2);
		add_filter( $filter, 'ses_set_sortable_columns' );
	}
}


function ses_populate_columns( $column, $id ) {

    if ( 'ses_meta' === $column ) {
		$status = esc_attr( get_post_meta( $id, 'ses_status', true ) );
		$comment = esc_attr( get_post_meta( $id, 'ses_comment', true ) );

		$color = 'red';
		switch($status) {
			case 'needsmore':
				$color = 'orange';
			break;
			case 'complete':
				$color = 'green';
			break;
		}
		?>

		<div style="float: left; position: relative; top: 5px; margin-right: 15px; width: 11px; height: 11px; border-radius:50%; background: <?php echo $color; ?>;"></div>
		<div><strong><?php echo $comment; ?></strong></div>

		<?php
    }
}

function ses_set_sortable_columns( $columns ) {
  $columns['ses_meta'] = 'ses_meta';

  return $columns;
}


function ses_meta_filtering() {
  global $typenow;
  global $wp_query;

      $sesstatus = array( 
      	'Färdig' => 'complete', 
      	'Behöver mer' => 'needsmore', 
      	'Ej påbörjad'	=> 'unfinished'
      );
      $current_sesstatus = '';
      if( isset( $_GET['ses-status'] ) ) {
        $current_sesstatus = $_GET['ses-status'];
      } ?>
      <select name="ses-status" id="ses-status">
        <option value="all" <?php selected( 'all', $current_sesstatus ); ?>><?php _e( 'All redaktionell status', 'wp-ses' ); ?></option>
        <?php foreach( $sesstatus as $key=>$value ) { ?>
          <option value="<?php echo esc_attr( $value ); ?>" <?php selected( $value, $current_sesstatus ); ?>><?php echo esc_attr( $key ); ?></option>
        <?php } ?>
      </select>
  <?php 
}
add_action( 'restrict_manage_posts', 'ses_meta_filtering' );


function ses_filter_query( $query ) {
  global $pagenow;
  $post_type = isset( $_GET['post_type'] ) ? $_GET['post_type'] : '';
  if ( is_admin() && $pagenow=='edit.php' && isset( $_GET['ses-status'] ) && $_GET['ses-status'] !='all' ) {
  	if ($_GET['ses-status'] == 'unfinished') {
	    $query->query_vars['meta_key'] = 'ses_status';
	    $query->query_vars['meta_value'] = '';
	    $query->query_vars['meta_compare'] = 'NOT EXISTS';  		
  	}
  	else {
	    $query->query_vars['meta_key'] = 'ses_status';
	    $query->query_vars['meta_value'] = $_GET['ses-status'];
	    $query->query_vars['meta_compare'] = '=';
	}
  }
}
add_filter( 'parse_query', 'ses_filter_query' );

